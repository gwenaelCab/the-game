#Glossaire

---

## Mécaniques de jeu {#MecaniquesJeu}

- <a name="niveau">**Niveau / Level**</a> : Un niveau est composée d'[objectifs](#objectif) à remplir pour le réussir. A la fin de chaque niveau, le [scénario](#scenario) est sauvegardé ou non, en fonction de la [difficulté](#difficulte) du [scénario](#scenario). A la fin de chaque niveau, un accès à l'[atelier](#atelier).
- <a name="objectif">**Goal**</a> : Une des conditions de réussite du [niveau](#niveau).
- **Tâche / Task** : Action coordonnée de plusieurs joueurs.
- <a name="difficulte">**Difficulté / Difficulty**</a> : facile, normal, difficile …
- **Événement aléatoire / Random Event** : Événement appliqué à la carte entière pouvant être positif ou négatif pour le [vaisseau](#vaisseau) exécutant la mission.
- <a name="scenario">**Scénario / Scenario**</a> : Suite de [niveaux](#niveau) leur donnant un contexte, une histoire. 
- <a name="equipage">**Équipage / Crew**</a> : Les joueurs d’une partie.
- <a name="role">**Rôle / Job**</a> : Fonction du [vaisseau](#vaisseau) attribué à un [poste](#poste).
- **Ennemie / Enemy** : entité hostile qui peut être pilotée par le système ou par d’autres joueurs qui attaque le [vaisseau](#vaisseau) de l’[équipage](#equipage). 
- **Bilan / Appraisal** : graphiques et informations rendu à l’[équipage](#equipage) en fin de [niveau](#niveau) et de [scénario](#scenario).

## Postes

- <a name="poste">**Poste / Post**</a> : Ensemble prédéfinie de [rôles](#role) assignés à un joueur.
- <a name="commandant">**Commandant / Major**</a> : Chargé de l'observation du champs proche du [char](#char), de son pilotage, et de la sélection du prochain [objectif](#objectif) à atteindre.
- <a name="artilleur">**Artilleur / Gunner**</a> : Chargé de la visée, du tir, et du rechargement du [canon](#canon).
- <a name="operateurRadio">**Opérateur radio / Radio Operator**</a> : Chargé de la communication des [objectifs](#objectif), du déclenchement des frappes d'artillerie, et du sabotage des communications ennemies.
- <a name="mecanicien">**Mécanicien / Mechanic**</a> : Chargé de la réparation des divers [équipements](#equipement) du char, de la sélection des rapports de la [transmission](#transmission), et de la sélection des munitions à mettre à disposition de l'[artilleur](#artilleur).

---

## Vaisseau {#Vaisseau}

- <a name="vaisseau">**Vaisseau / Ship**</a> : véhicule transportant l’[équipage](#equipage).
- <a name="atelier">**Atelier / Workshop**</a> : Écran permettant au commandant de vaisseau d'acheter / de réparer et / ou d'[améliorer](#amelioration) certains [équipements](#equipement) du [vaisseau](#vaisseau). Permet aux autres joueurs d'[améliorer](#amelioration) les [équipements](#equipement) courants associés à leur [poste](#poste).
- <a name="amelioration">**Amélioration / Upgradre**</a> : changement d’un élément du [vaisseau](#vaisseau) modifiant ses caractéristiques en jeu. Une amélioration ne peut être faite que dans l'[atelier](#atelier).
- <a name="equipement">**Equipement / Equipment**</a> : élément à part entière du [vaisseau](#vaisseau) ( exemple : radio, tourelle, etc)
- **Équipement hostile / Hostile Equipment** : [Équipement](#equipement) cherchant à nuire au [vaisseau](#vaisseau).

### Char {#char}

- <a name="char">**Char / Tank**</a> :  [Vaisseau](#vaisseau) composé d'un chef de char/conducteur, mécanicien, tireur, et d'un opérateur radio.
- <a name="canon">**Canon / Cannon**</a> : Le canon est l'arme principale du [char](#char). Elle est montée sur la [tourelle](#tourelle).
- <a name="tourelle">**Tourelle / Gun Turret**</a> : Partie mobile du [char](#char) sur laquelle est montée un [canon](#canon). L'artilleur ne voit la carte que par son biais.
- <a name="moteur">**Moteur / Engine** </a> : Élément primaire permettant de générer le mouvement du [char](#char).
- <a name="transmission">**Transmission / Transmission**</a> : Élément secondaire permettant de gérer la vitesse du [char](#char). C'est grâce à cette dernière que le char peut passer des vitesses.
- <a name="caisse">**Caisse / Body**</a> : Partie principale du char, située sous la [tourelle](#tourelle) et entre les [chenilles](#chenilles), elle contient l'[équipage](#equipage) et les parties motrices du char.
- <a name="chenilles">**Chenilles / Tracks**</a> : Équivalent des roues pour un véhicule... à roues.
- <a name="obus">**Obus / Shell**</a> : Projectile tiré par le [canon](#canon) du char.
- **Culasse / Breech** : La culasse est le système entourant la chambre dans laquelle les [obus](#obus) sont chargés dans le but d'être tirés. On en extrait les douilles.
- **Couronne / Turret Ring** : La couronne est le dispositif permettant la rotation de la [tourelle](#tourelle). C'est une cible de choix sur un [char](#char).
