extends "res://addons/gut/test.gd"

var Mechanic = load("res://Scripts/Mechanic.gd")
var Major = preload("res://Scripts/Major.gd")
var GameState = load("res://Scripts/gamestate.gd")

const epsilon = 0.001
var mec

func before_each() -> void:
	var gameState = GameState.new()
	gameState.createServer(get_tree())
	mec = Mechanic.new()


func test_setSpeedInRange() -> void:
	mec._on_Speedl_value_changed(100);
	assert_almost_eq(global_data.getPlayerSpeed(), 100.0, epsilon,
	"Speed can be set to an arbitrary value in the range.")

func test_setSpeedTooHigh() -> void:
	mec._on_Speedl_value_changed(9800);
	assert_almost_eq(global_data.getPlayerSpeed(), float(global_data.MAX_SPEED),
	epsilon, "The speed can't be higher than the max speed.")

func test_setNegativeSpeed() -> void:
	mec._on_Speedl_value_changed(-4);
	assert_almost_eq(global_data.getPlayerSpeed(), 0.0, epsilon,
	"The speed can't be higher than the max speed.")
