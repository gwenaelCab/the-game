extends "res://addons/gut/test.gd"

var RadioOperatorScene = load('res://Scenes/Radio-Operator.tscn')
var RadioOperator = null
var virtualTree = null
var treeRoot = null

func before_each():
	RadioOperator = partial_double(RadioOperatorScene, DOUBLE_STRATEGY.FULL).instance()
	virtualTree = partial_double(SceneTree).new()
	treeRoot = virtualTree.get_root()
	stub(RadioOperator,'get_tree').to_return(virtualTree)
	stub(RadioOperator,'get_viewport').to_return(get_viewport())
	
	# We don't want to test enemies
	RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Paths").free()
	RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Paths2").free()
	RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/EnnemyTank").free()

func after_each():
	RadioOperator = null
	virtualTree = null

func test_tank_camera_removed():
	assert_true(RadioOperator.has_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank/Camera2D"))
	RadioOperator._ready()
	assert_false(RadioOperator.has_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank/Camera2D"), "test tank camera is removed")

func test_toggle_AirStrike_when_not_ready():
	var text = RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").text
	RadioOperator._AirStrike_reloadProgress = 50
	RadioOperator._toggle_AirStrike()
	assert_false(RadioOperator._AirStrike_enabled, "test toggle airstrike when reload isn't finished")
	assert_eq(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").text, text, "test text not change on airstrike toggle when reload not finished")

func test_toggle_AirStrike_when_ready():
	var text = RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").text
	RadioOperator._AirStrike_reloadProgress = 100
	RadioOperator._toggle_AirStrike()
	assert_true(RadioOperator._AirStrike_enabled, "test toggle airstrike when reload is finished")
	assert_ne(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").text, text, "test text change on airstrike toggle when reload finished")

func test_hide_target_cursor():
	RadioOperator._hide_target_cursor()
	assert_eq(Input.get_mouse_mode(), Input.MOUSE_MODE_VISIBLE, "test mouse mode visible")
	assert_false(RadioOperator.get_node("targetCursor").visible, "test target cursor not visible")

func test_target_cursor_following_mouse():
	simulate(RadioOperator, 10, .1)
	assert_eq(RadioOperator.get_node("targetCursor").position, get_viewport().get_mouse_position(), "test update target cursor position")

func test_on_mouse_exited():
	RadioOperator._on_mouse_exited()
	assert_called(RadioOperator, "_hide_target_cursor")

func test_on_mouse_entered_reload_not_ready():
	RadioOperator._AirStrike_reloadProgress = 50
	RadioOperator._AirStrike_enabled = true
	RadioOperator._on_mouse_entered()
	assert_not_called(RadioOperator, "_show_target_cursor")
	RadioOperator._AirStrike_enabled = false
	RadioOperator._on_mouse_entered()
	assert_not_called(RadioOperator, "_show_target_cursor")

func test_on_mouse_entered_airstrike_not_enabled():
	RadioOperator._AirStrike_enabled = false
	RadioOperator._AirStrike_reloadProgress = 100
	RadioOperator._on_mouse_entered()
	assert_not_called(RadioOperator, "_show_target_cursor")
	RadioOperator._AirStrike_reloadProgress = 50
	RadioOperator._on_mouse_entered()
	assert_not_called(RadioOperator, "_show_target_cursor")

func test_on_mouse_entered_airstrike_ready():
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 100
	RadioOperator._on_mouse_entered()
	assert_called(RadioOperator, "_show_target_cursor")
	RadioOperator._hide_target_cursor()

func test_on_input_event_reload_not_ready():
	var input = InputEventMouseButton.new()
	input.pressed = true
	input.button_index = BUTTON_LEFT
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 50
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_not_called(RadioOperator, "_toggle_AirStrike")

func test_on_input_event_airstrike_not_enabled():
	var input = InputEventMouseButton.new()
	input.pressed = true
	input.button_index = BUTTON_LEFT
	RadioOperator._AirStrike_enabled = false
	RadioOperator._AirStrike_reloadProgress = 100
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_not_called(RadioOperator, "_toggle_AirStrike")

func test_on_input_event_input_button_not_left():
	var input = InputEventMouseButton.new()
	input.pressed = true
	input.button_index = BUTTON_RIGHT
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 100
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_not_called(RadioOperator, "_toggle_AirStrike")

func test_on_input_event_input_not_pressed():
	var input = InputEventMouseButton.new()
	input.pressed = false
	input.button_index = BUTTON_LEFT
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 100
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_not_called(RadioOperator, "_toggle_AirStrike")

func test_on_input_event_input_not_mouse_button():
	var input = InputEventMouseMotion.new()
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 100
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_not_called(RadioOperator, "_toggle_AirStrike")

func test_on_input_event_valid():
	var input = InputEventMouseButton.new()
	input.pressed = true
	input.button_index = BUTTON_LEFT
	RadioOperator._AirStrike_enabled = true
	RadioOperator._AirStrike_reloadProgress = 100
	
	RadioOperator._on_input_event(RadioOperator.get_viewport(), input, 0)
	assert_called(RadioOperator, "_toggle_AirStrike")

func test_rotation_update():
	global_data.player_rotation = 50.4
	assert_almost_ne(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank").rotation, 50.4, .1, "test rotation init not 50.4")
	simulate(RadioOperator, 100, .1)
	assert_almost_eq(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank").rotation, 50.4, .1, "test rotation update to 50.4 on process")

func test_position_update():
	global_data.player_position = Vector2(50.4, 6.1)
	assert_ne(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank").position, Vector2(50.4, 6.1), "test position init not (50.4, 6.1)")
	simulate(RadioOperator, 100, .1)
	assert_eq(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank").position, Vector2(50.4, 6.1), "test position update to (50.4, 6.1) on process")

func test_animation_playing_update():
	global_data.player_is_moving = true
	assert_false(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank/TankBody/BodyTank").playing, "test animation playing init not playing")
	simulate(RadioOperator, 100, .1)
	assert_true(RadioOperator.get_node("Content/Up Screen/MinimapContainer/Minimap/Map/Tank/TankBody/BodyTank").playing, "test animation playing update to playing on process")

func test_reloading():
	RadioOperator._AirStrike_reloadProgress = 50.0
	simulate(RadioOperator, 10, .4)
	assert_almost_ne(RadioOperator._AirStrike_reloadProgress, 50.0, .1, "test reloading")
	simulate(RadioOperator, 500, .4)
	assert_almost_eq(RadioOperator._AirStrike_reloadProgress, 100.0, .1, "test reload stop at 100")

func test_progressbar_update_on_reloading():
	RadioOperator._AirStrike_reloadProgress = 50.0
	RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikePB").value = 0
	simulate(RadioOperator, 10, .4)
	assert_ne(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikePB").value, 0, "test reload progress bar value changed")
	assert_almost_eq(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikePB").value, RadioOperator._AirStrike_reloadProgress, .5, "test reload progress bar equals reload progress")

func test_airstrike_button_disabled_when_reloading():
	RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").disabled = false
	RadioOperator._AirStrike_reloadProgress = 50.0
	simulate(RadioOperator, 10, .4)
	assert_true(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").disabled, "test airstrike button disabled when reloading")

func test_airstrike_button_enabled_when_ready():
	RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").disabled = true
	RadioOperator._AirStrike_reloadProgress = 100.0
	simulate(RadioOperator, 10, .4)
	assert_false(RadioOperator.get_node("Content/Up Screen/Controls/Special Attacks/AirStrikeBtn").disabled, "test airstrike button enabled when ready")

