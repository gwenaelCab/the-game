extends "res://addons/gut/test.gd"

var gameClass = load('res://Scripts/Game.gd')
var game = null
var virtualTree = null
var treeRoot = null

func before_each():
	game = partial_double(gameClass, DOUBLE_STRATEGY.FULL).new()
	virtualTree = partial_double(SceneTree).new()
	treeRoot = virtualTree.get_root()
	stub(game,'get_tree').to_return(virtualTree)

func after_each():
	game = null
	virtualTree = null

func test_endGame_victory():
	game.endGame(true)
	assert_true(treeRoot.has_node("Victory"), "test that victory screen is displayed in case of victory")
	assert_false(treeRoot.has_node("Defeat"), "test that defeat screen is not displayed in case of victory")

func test_endGame_defeat():
	game.endGame(false)
	assert_true(treeRoot.has_node("Defeat"), "test that defeat screen is displayed in case of defeat")
	assert_false(treeRoot.has_node("Victory"), "test that victory screen is not displayed in case of defeat")

func test_endGame_remove_game():
	game.endGame(true)
	assert_false(treeRoot.has_node("Game"), "test that game screen is not displayed after end of game")
	game.endGame(false)
	assert_false(treeRoot.has_node("Game"), "test that game screen is not displayed after end of game")

func test_game_exit_on_escape():
	var inputEvent = InputEventAction.new()
	inputEvent.action = "ui_cancel"
	inputEvent.pressed = true
	Input.parse_input_event(inputEvent)
	simulate(game,1,.1)
	assert_called(virtualTree, 'quit')
	inputEvent = null
