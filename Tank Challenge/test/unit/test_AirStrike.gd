extends "res://addons/gut/test.gd"

var AirStrikeScene = load('res://Scenes/AirStrike.tscn')
var AirStrike = null

func before_each():
	AirStrike = partial_double(AirStrikeScene, DOUBLE_STRATEGY.FULL).instance()
	stub(AirStrike, 'queue_free').to_do_nothing()

func after_each():
	AirStrike = null

func test_bomb_start():
	AirStrike.start(Vector2(5, 255))
	assert_eq(AirStrike.global_position, Vector2(5, 255), "test bomb position after start")
	assert_called(AirStrike, "explosion")

func test_bomb_explosion_animation():
	AirStrike.explosion()
	assert_true(AirStrike.get_node("Explosion").is_playing(), "test bomb explosion animation playing")

func test_bomb_explosion_removed():
	AirStrike.explosion()
	AirStrike.get_node("Explosion").emit_signal("animation_finished")
	assert_called(AirStrike, "queue_free")
