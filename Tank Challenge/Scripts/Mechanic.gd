extends Control

var gearNumber: int = 6

func _ready():
	global_data.connect("player_health_changed", self, "updateHealthColor")
	$"Content/Speed control/Control".max_value = global_data.MAX_SPEED
	$"Content/Speed control/Control".step = global_data.MAX_SPEED / gearNumber
	$"Content/Speed control/Control".value = global_data.getPlayerSpeed()

func _process(delta):
	get_node("Content/Right Screen/Health/Health Details/Health4").text = String(global_data.getPlayerHealth())
	$"Content/Right Screen/Health/Health Details/Repair4".disabled = (global_data.getPlayerHealth() == 100 || global_data.getPlayerScrap() == 0)
	$"Content/Right Screen/Resources/Value".text = String(global_data.getPlayerScrap())

func repair():
	var scrap = global_data.getPlayerScrap();
	var maxHealed = floor(scrap * (1 / global_data.SCRAP_PER_HP))
	var toBeHealed = Major.MAX_HEALTH - global_data.getPlayerHealth()
	if (maxHealed < toBeHealed):
		global_data.setPlayerHealth(global_data.getPlayerHealth() + maxHealed)
		scrap = scrap - (maxHealed * global_data.SCRAP_PER_HP)
	else:
		global_data.setPlayerHealth(global_data.getPlayerHealth() + toBeHealed)
		scrap = scrap - (toBeHealed * global_data.SCRAP_PER_HP)
	global_data.setPlayerScrap(scrap)
	
func updateHealthColor() -> void :
	var health = global_data.getPlayerHealth()
	var newColor : Color
	if(health <= 0):
		newColor = Color(0, 0, 0)
	else:
		var redCompute = (Major.MAX_HEALTH - health) / float(Major.MAX_HEALTH)
		var greenCompute = health / float(Major.MAX_HEALTH)
		newColor = Color(redCompute, greenCompute, 0)
	get_node("Content/Left Screen/Tank").modulate = newColor

func _on_BodyRepair_pressed():
	repair()

func _on_Speedl_value_changed(value):
	global_data.setPlayerSpeed(value)
