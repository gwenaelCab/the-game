extends Node

signal shot_fired
signal reloaded
signal player_health_changed
signal player_speed_changed

const MAX_SPEED = 175
const SCRAP_PER_HP = 1.2

remotesync var player_health: int = 100
remotesync var player_speed: float = 100
remotesync var player_position: Vector2 = Vector2()
remotesync var player_rotation: float = 0.0
remotesync var player_turret_rotation: float = 0.0
remotesync var player_is_moving: bool = false
remotesync var player_scrap: int = 20

var ennemiesTurretRotation: Array = Array()
var ennemiesHealth: Array = Array()
var ennemiesCount: int = 0

func reset() -> void:
	player_speed = 100
	player_health = 100
	player_position = Vector2()
	player_rotation = 0.0
	player_turret_rotation = 0.0
	player_is_moving = false
	player_scrap = 20
	ennemiesTurretRotation = Array()
	ennemiesHealth = Array()
	ennemiesCount = 0
	

func setPlayerHealth(value: int) -> void:
	rset("player_health", value)
	rpc("playerHealthChanged")

func setPlayerSpeed(speed: float) -> void:
	if(speed > MAX_SPEED):
		speed = MAX_SPEED
	elif(speed < 0):
		speed = 0
	rset("player_speed",speed)
	rpc("playerSpeedChanged")

func getPlayerSpeed() -> float:
	return player_speed

remotesync func playerSpeedChanged():
	emit_signal("player_speed_changed")

remotesync func playerHealthChanged():
	emit_signal("player_health_changed")

func getPlayerHealth() -> int:
	return player_health

func setPlayerPosition(value: Vector2) -> void:
	rset_unreliable("player_position", value)

func getPlayerPosition() -> Vector2:
	return player_position

func setPlayerRotation(value: float) -> void:
	rset_unreliable("player_rotation", value)

func getPlayerRotation() -> float:
	return player_rotation

func setPlayerIsMoving(value: bool) -> void:
	rset("player_is_moving", value)

func playerIsMoving() -> bool:
	return player_is_moving

func setPlayerTurretRotation(value: float) -> void:
	rset_unreliable("player_turret_rotation", value)

func getPlayerTurretRotation() -> float:
	return player_turret_rotation

remotesync func player_fire() -> void:
	if (!(gamestate.get_player_role() in ["Mechanic", "Radio Operator"])):
		emit_signal("shot_fired")

func addEnnemy() -> int:
	ennemiesTurretRotation.append(0)
	ennemiesHealth.append(100)
	ennemiesCount += 1
	return ennemiesTurretRotation.size() - 1

func removeEnnemy() -> int:
	ennemiesCount -= 1
	return ennemiesCount

func getEnnemyLastId() -> int:
	return ennemiesTurretRotation.size() - 1

remotesync func setEnnemyTurretRotation(ennemy_id: int, value: float) -> void:
	if (gamestate.get_player_role() != "Mechanic"):
		ennemiesTurretRotation[ennemy_id] = value

func getEnnemyTurretRotation(ennemy_id: int) -> float:
	return ennemiesTurretRotation[ennemy_id]

remotesync func setEnnemyHealth(ennemy_id: int, value: int) -> void:
	if (gamestate.get_player_role() != "Mechanic"):
		ennemiesHealth[ennemy_id] = value

func getEnnemyHealth(ennemy_id: int) -> int:
	return ennemiesHealth[ennemy_id]

func getEnnemiesCount() -> int:
	return ennemiesCount

func getPlayerScrap() -> int:
	return player_scrap
	
func setPlayerScrap(value: int) -> void:
	rset("player_scrap", value)

remotesync func AirStrike(position: Vector2):
	var bomb = preload("res://Scenes/AirStrike.tscn").instance()
	bomb.start(position)
	if (gamestate.get_player_role() != "Mechanic"):
		get_tree().get_root().find_node("Map", true, false).add_child(bomb)
