extends KinematicBody2D

var damage : int = 25

func explosion():
	$Explosion.play()
	yield($Explosion, 'animation_finished')
	queue_free()

func start(bombPosition):
	global_position = bombPosition
	explosion()
