extends Area2D

signal collision_damage

func _ready():
	self.connect("collision_damage", self.get_parent().get_parent(), "_on_collision_damage")
	
func _on_areaDetectionTankBody_body_entered(body):
	if(!body.is_in_group("Player") && !body.is_in_group("no_collide")):
		emit_signal("collision_damage", body)
