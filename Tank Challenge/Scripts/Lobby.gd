extends Control


onready var _host_btn = $Panel/VBoxContainer/HBoxContainer2/HBoxContainer/Host
onready var _connect_btn = $Panel/VBoxContainer/HBoxContainer2/HBoxContainer/Connect
onready var _disconnect_btn = $Panel/VBoxContainer/HBoxContainer2/HBoxContainer/Disconnect
onready var _name_edit = $Panel/VBoxContainer/HBoxContainer/NameEdit
onready var _host_edit = $Panel/VBoxContainer/HBoxContainer2/Hostname
onready var _game = $Panel/VBoxContainer/Game

func _ready() -> void:
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#Connect gamestate signals to lobby functions
	gamestate.connect("connection_failed", self, "_on_connection_failed")
	gamestate.connect("connection_succeeded", self, "_on_connection_success")
	gamestate.connect("player_list_changed", self, "refresh_lobby")
	gamestate.connect("game_ended", self, "_on_game_ended")
	gamestate.connect("game_error", self, "_on_game_error")
	gamestate.connect("name_already_taken",self,"same_Name")


func _on_Host_pressed() -> void:
	#Don't host if no name is entered
	if _name_edit.text == "":
		return

	_host_btn.disabled = true
	_name_edit.editable = false
	_host_edit.editable = false
	_connect_btn.hide()
	_disconnect_btn.show()
	var player_name = _name_edit.text
	gamestate.host_game(player_name)
	refresh_lobby()


func _on_Connect_pressed() -> void:
	#Don't join if no name is entered
	if _name_edit.text == "":
		return
	
	var ip
	if(_host_edit.text.empty()):
		ip = _host_edit.placeholder_text
	else:
		ip = _host_edit.text

	get_node("Panel/VBoxContainer/Game/HBoxContainer/RichTextLabel").text=""
	_host_edit.editable = false
	_connect_btn.hide()
	_host_btn.disabled = true
	_disconnect_btn.show()
	_name_edit.editable = false
	var player_name = _name_edit.text
	gamestate.join_game(ip, player_name)
	# refresh_lobby() gets called by the player_list_changed signal

func _on_Disconnect_pressed() -> void:
	if get_tree().is_network_server():
		get_tree().emit_signal("server_disconnected")
	gamestate.end_game()
	get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer/ItemList").clear()
	get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer2/ItemList").clear()

func _on_connection_success() -> void:
	_host_btn.disabled = true

func _on_connection_failed() -> void:
	_host_btn.disabled=false
	_connect_btn.disabled=false

func _on_game_ended() -> void:
	show()
	_disconnect_btn.hide()
	_connect_btn.show()
	_host_btn.disabled = false
	_host_edit.editable = true
	_name_edit.editable = true
	get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer/ItemList").clear()
	get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer2/ItemList").clear()
	
func _on_game_error() -> void:
	gamestate.end_game()
	
func same_Name() -> void:
	var popup = get_node("AcceptDialog")
	popup.visible = true
	popup.dialog_text = "Same name as another player"
	popup.popup()
	gamestate.end_game()

func refresh_lobby() -> void:
	var playerList = get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer/ItemList")
	var chooseRoleList = get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer2/ItemList")
	var players = gamestate.get_player_list()
	var playerRoles = gamestate.get_player_roles()

	playerList.clear()
	playerList.add_item(gamestate.get_player_name() + " (You) - " + gamestate.get_player_role())
	
	for p_num in range(players.size()):
		var p = players[p_num]
		var r = gamestate.ALL_ROLES[playerRoles[p_num]]

		if (not get_tree().is_network_server() and p!=gamestate.get_player_name()) or (get_tree().is_network_server() and p_num!=0):
			playerList.add_item(p + " - " +r)

	chooseRoleList.clear()
	for role in range(gamestate.ALL_ROLES.size()):
		chooseRoleList.add_item(gamestate.ALL_ROLES[role])
		chooseRoleList.set_item_disabled(role,!gamestate.isRoleAvailable(role))
	get_node("Panel/VBoxContainer/Game/HBoxContainer/VBoxContainer/Action").disabled=not get_tree().is_network_server()


func _on_start_pressed() -> void:
	gamestate.begin_game()
	global_data.reset()


func _on_Hostname_text_changed(new_text):
	if new_text.is_valid_ip_address() || new_text in ['localhost', '']:
		$Panel/VBoxContainer/HBoxContainer2/Hostname.add_color_override("font_color", '#ffffff')
	else:
		$Panel/VBoxContainer/HBoxContainer2/Hostname.add_color_override("font_color", '#ff0000')
	
